<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style type="text/css">
        .funbox {
            box-shadow: 0 2px 14px rgba(0, 0, 0, 0.14);
            max-width: 500px;
        }

        /* fix bullet position */
        /* sucks, but use this to position based on font size */
        .fa-li {
            top: 20%;
        }
    </style>
</head>
<body>

<div class="mx-auto mt-5 p-5 funbox">
    <h1 class="display-4">Test supri</h1>
    <h1 class="text-muted pb-2"><small>Laravel</small></h1>
    <hr>
    <ul class="fa-ul">
          <a class="nav-link text-center mr-2" href="{{ route('login') }}"><span class="fw-bold">login</span></a>
          <a class="nav-link text-center mr-2" href="{{ route('register') }}"><span class="fw-bold">registration</span></a>
    </ul>
</div>

</div>

</body>
</html>