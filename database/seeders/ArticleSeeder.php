<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Article;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $articles = [
            [
                'title' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry", 
                'header' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
                'body' => htmlentities("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."),
                'footer' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
                'image_url' => "dummy.jpg",
                'tag' => "dummy",
                'slug' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
                'writer' => "dummy"
            ],
            [
                'title' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry", 
                'header' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry?",
                'body' => htmlentities("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."),
                'footer' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
                'image_url' => "dummy.jpg",
                'tag' => "dummy",
                'slug' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
                'writer' => "dummy"
            ],

            // Tools for slug : https://blog.tersmitten.nl/slugify/
            // Tools for html tag : https://wordhtml.com/
            // [
            //     'title' => "", 
            //     'header' => "",
            //     'body' => ``,
            //     'footer' => "",
            //     'image_url' => "",
            //     'tag' => "",
            //     'slug' => "",
            //     'writer' => ""   
            // ]
        ];

        foreach($articles as $article){
            Article::create($article);
        }
    }
}
