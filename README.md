# Petunjuk Set-up
## Awal Clone/pull
- Masuk ke folder project
- Gunakan perintah "composer install"
- Buat copy dari file yang namanya .env.example lalu ganti namanya menjadi .env
- lalu jalankan "php artisan key:generate" untuk membuat kode (untuk encoding)
- coba run dengan "php artisan serve"

## Proses Pengerjaan dengan Backend DB
- "git pull origin <nama_branch>" diusahakan setiap kali sebelum mengerjakan sesuatu agar mengikuti update terbaru dahulu
- isilah variabel konstanta DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, dan DB_PASSWORD sesuai keterangan database, jika menggunakan mySQL ubah DB_DATABASE saja, buat database baru di mysql
- untuk menyamakan database run "php artisan migrate"
-untuk menjalankan seeder nya "php artisan migrate:refresh --seed"
- lalu jalankan kembali dengan "php artisan serve"

## Proses Pengerjaan dengan Backend DB yang Telah Ada Isi Default (Seeder)
- Gunakan perintah "php artisan migrate:refresh --seed"
- cek db (seperti localhost/phpmyadmin) apakah sudah ada isi defaultnya?
- lalu jalankan kembali dengan "php artisan serve"
- pilih perubahan mana yang ingin disimpan dengan menggunaka bantuan dari editor vscode atau "git mergetool"
- lalu push kembali ke branch dengan "git push -u origin <nama_branch>"
